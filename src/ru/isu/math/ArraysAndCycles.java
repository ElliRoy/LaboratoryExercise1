package ru.isu.math;

public class ArraysAndCycles {
    //В этом методе выбирается задание для выолнения
    public static void main(String[] args) {
        System.out.println("What task do you want to check? (1-10)");
        int Switcher = Instruments.readInteger("Enter a number of task: ");
        switch (Switcher) {
            case 1:
                Tasks.getSortSequenceOrNot(Instruments.readInteger("Enter a number of elements in a sequence: "));
                break;
            case 2:
                Tasks.sortRowsOfSquareIntegerMatrixInAscendingOrder(Instruments.readInteger("Enter a dimension of matrix: "));
                break;
            case 3:
                Tasks.sortColumnOfSquareIntegerMatrixInAscendingOrder(Instruments.readInteger("Enter a dimension of matrix: "));
                break;
            case 4:
                Tasks.findMinimumSumOfDiagonalElementsParallelSideDiagonal(Instruments.readInteger("Enter a dimension of square matrix: "));
                break;
            case 5:
                Tasks.findMaximumSumOfDiagonalElementsParallelMainDiagonal(Instruments.readInteger("Enter a dimension of square matrix: "));
                break;
            case 6:
                Tasks.checkDoesFirstMatrixBeElementOfSecondMatrix(Instruments.readInteger("Enter a dimension of first matrix: "), Instruments.readInteger("Enter a dimension of second matrix: "));
                break;
            case 7:
                Tasks.checkDoesFirstSequenceBeElementOfSecondSequnce(Instruments.readInteger("Enter a dimesion of first sequence: "), Instruments.readInteger("Enter a dimension of second sequence: "));
                break;
            case 8:
                Tasks.getMultiplicationOfTwoMatrix(Instruments.readInteger("Enter a dimension of first matrix: "), Instruments.readInteger("by: "), Instruments.readInteger("Enter dimension of second matrix: "), Instruments.readInteger("by: "));
                break;
            case 9:
                Tasks.findTheMostDistancePoint(Instruments.readInteger("Enter the number of points: "));
                break;
            case 10:
                Tasks.calculateTheLengthOfTheBrokenLine(Instruments.readInteger("Enter the number of points: "));
        }
    }
}
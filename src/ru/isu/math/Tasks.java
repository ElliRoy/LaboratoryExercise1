package ru.isu.math;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Math.pow;


public class Tasks {
    //1 задание
    public static void getSortSequenceOrNot(int n) {
        System.out.println("The elements of the sequence: ");
        int[] Sequence;
        Instruments.writeArray(Sequence = Instruments.generateArray1(n));
        System.out.println();
        System.out.println();
        for (int i = 0; i < Sequence.length; i++) {
            if (Sequence[i] > Sequence[i + 1]) {
                System.out.println("***************************************");
                System.out.println("Array is not sorted in ascending order!");
                i = Sequence.length;
            }
        }
    }

    //2 задание
    public static void sortRowsOfSquareIntegerMatrixInAscendingOrder(int n) {
        int[][] SquareMatrix;
        int i, j, k, N;
        System.out.println();
        Instruments.writeArray(SquareMatrix = Instruments.generateArray(n, n));

        int RowSum = 0;
        int[] ArrayOfRowSums = new int[n];
        System.out.println("Sums of each row: ");
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                RowSum = SquareMatrix[i][j] + RowSum;
            }
            ArrayOfRowSums[i] = RowSum;
            System.out.println("Row " + (i + 1) + ": " + RowSum);
            RowSum = 0;
        }

        System.out.println();

        int[] MemoryArray = new int[n];
        for (N = 0; N < n; N++) {
            for (k = 1; k < n; k++) {
                if (ArrayOfRowSums[k - 1] > ArrayOfRowSums[k]) {
                    for (j = 0; j < n; j++) {
                        MemoryArray[j] = SquareMatrix[k - 1][j];
                        SquareMatrix[k - 1][j] = SquareMatrix[k][j];
                        SquareMatrix[k][j] = MemoryArray[j];
                    }
                    MemoryArray[0] = ArrayOfRowSums[k - 1];
                    ArrayOfRowSums[k - 1] = ArrayOfRowSums[k];
                    ArrayOfRowSums[k] = MemoryArray[0];
                }
            }
        }

        System.out.println("The rows of square matrix are sorted in ascending order:");
        Instruments.writeArray(SquareMatrix);

        System.out.println("Sums of each row: ");
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                RowSum = SquareMatrix[i][j] + RowSum;
            }
            ArrayOfRowSums[i] = RowSum;
            System.out.println("Row " + (i + 1) + ": " + RowSum);
            RowSum = 0;
        }
    }

    //3 задание
    public static void sortColumnOfSquareIntegerMatrixInAscendingOrder(int n) {
        int[][] SquareMatrix;
        int i, j, k, N;
        Instruments.writeArray(SquareMatrix = Instruments.generateArray(n, n));

        int ColumnSum = 0;
        int[] ArrayOfColumnSums = new int[n];
        System.out.println("Sums of each column: ");
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                ColumnSum = SquareMatrix[j][i] + ColumnSum;
            }
            ArrayOfColumnSums[i] = ColumnSum;
            System.out.println("Column " + (i + 1) + ": " + ColumnSum);
            ColumnSum = 0;
        }

        System.out.println();
        int[] MemoryArray = new int[n];
        for (N = 0; N < n; N++) {
            for (k = 1; k < n; k++) {
                if (ArrayOfColumnSums[k - 1] > ArrayOfColumnSums[k]) {
                    for (j = 0; j < n; j++) {
                        MemoryArray[j] = SquareMatrix[j][k - 1];
                        SquareMatrix[j][k - 1] = SquareMatrix[j][k];
                        SquareMatrix[j][k] = MemoryArray[j];
                    }
                    MemoryArray[0] = ArrayOfColumnSums[k - 1];
                    ArrayOfColumnSums[k - 1] = ArrayOfColumnSums[k];
                    ArrayOfColumnSums[k] = MemoryArray[0];
                }
            }
        }

        System.out.println("The rows of square matrix are sorted in ascending order:");
        Instruments.writeArray(SquareMatrix);

        System.out.println("Sums of each column: ");
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                ColumnSum = SquareMatrix[j][i] + ColumnSum;
            }
            ArrayOfColumnSums[i] = ColumnSum;
            System.out.println("Column " + (i + 1) + ": " + ColumnSum);
            ColumnSum = 0;
        }
    }

    //4 задание
    public static void getMultiplicationOfTwoMatrix(int m, int n, int n2, int k) {
        if (n != n2) {
            System.out.println();
            System.out.println("**************************\nA product can not be done!");
            System.out.println("Error: Check dimensions of matrixes!\nIf first matrix is m x n then second must be n x k.");
            System.out.println();
            String[] ReturnArray = new String[1];
            ReturnArray[0] = "";
            ArraysAndCycles.main(ReturnArray);
        }

        int[][] FirstMatrix = new int[m][n];
        int[][] SecondMatrix = new int[n2][k];
        int[][] ProductMatrix = new int[m][k];

        FirstMatrix = Instruments.generateArray(m, n);
        SecondMatrix = Instruments.generateArray(n, k);
        Instruments.writeArray(FirstMatrix);
        Instruments.writeArray(SecondMatrix);

        int ElementIJ = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                for (int l = 0; l < n; l++) {
                    ElementIJ = FirstMatrix[i][l] * SecondMatrix[l][j] + ElementIJ;
                }
                ProductMatrix[i][j] = ElementIJ;
                ElementIJ = 0;
            }
            ElementIJ = 0;
        }

        System.out.println("The matrix of product:");
        Instruments.writeArray(ProductMatrix);
    }

    //5 задание
    public static void findMinimumSumOfDiagonalElementsParallelSideDiagonal(int n) {
        int[][] SquareMatrix;
        Instruments.writeArray(SquareMatrix = Instruments.generateArray(n, n));

        int j, k;
        int SumDiagonal;
        int minSum = SquareMatrix[0][0];

        for (j = 0; j < n; j++) {
            SumDiagonal = SquareMatrix[0][j];
            for (k = 1; k < j + 1; k++) {
                SumDiagonal = SquareMatrix[k][j - k] + SumDiagonal;
            }
            if (SumDiagonal < minSum) minSum = SumDiagonal;
        }
        System.out.println(minSum);
        for (j = 1; j < n; j++) {
            if (j == n - 1) {
                SumDiagonal = SquareMatrix[n - 1][n - 1];
            } else {
                SumDiagonal = SquareMatrix[n - 1][j];
                for (k = 0; k < n - j; k++) {
                    SumDiagonal = SquareMatrix[n - 1 - k][j + k] + SumDiagonal;
                }
            }
            if (SumDiagonal < minSum) minSum = SumDiagonal;
        }

        System.out.println("The most minimum sum is " + minSum);
    }

    //6 задание
    public static void findMaximumSumOfDiagonalElementsParallelMainDiagonal(int n) {
        int[][] SquareMatrix;
        Instruments.writeArray(SquareMatrix = Instruments.generateArray(n, n));

        int j, k;
        int SumDiagonal;
        int maxSum = SquareMatrix[0][0];

        for (j = 0; j < n; j++) {
            SumDiagonal = SquareMatrix[0][j];
            for (k = 1; k < n - j; k++) {
                SumDiagonal = SquareMatrix[k][k + j] + SumDiagonal;
            }
            if (SumDiagonal > maxSum) maxSum = SumDiagonal;
        }
        System.out.println(maxSum);
        for (j = 0; j < n; j++) {
            if (j == n - 1) {
                SumDiagonal = SquareMatrix[n - 1][n - 1];
            } else {
                SumDiagonal = SquareMatrix[n - 1][j];
                for (k = 1; k < j + 1; k++) {
                    SumDiagonal = SquareMatrix[n - 1 - k][j - k] + SumDiagonal;
                }
            }
            if (SumDiagonal > maxSum) maxSum = SumDiagonal;
        }

        System.out.println("The most maximum sum is " + maxSum);
    }

    //7 задание
    public static void checkDoesFirstMatrixBeElementOfSecondMatrix(int n, int m) {
        int[][][][] SecondMatrix = new int[n][n][m][m];
        int[][] FirstMatrix = new int[m][m];
        int From = Instruments.getRangeForRandom("From");
        int To = Instruments.getRangeForRandom("To");
        int i, j, k, l;

        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                for (k = 0; k < m; k++) {
                    for (l = 0; l < m; l++) {
                        SecondMatrix[i][j][k][l] = Instruments.getRandomInt(From, To);
                        if ((i == 1) && (j == 1)){
                            FirstMatrix[k][l] = SecondMatrix[i][j][k][l];
                        }
                    }
                }
            }
        }

        for (i = 0; i < n; i++){
            for (j = 0; j < n; j++){
                System.out.print(Arrays.deepEquals(SecondMatrix[i][j], FirstMatrix) + " ");
            }
        }
    }

    //8 задание
    public static void checkDoesFirstSequenceBeElementOfSecondSequnce(int n, int m) {
        int[][] SecondSequence = new int[n][m];
        int[] FirstSequence = new int[m];
        int From = Instruments.getRangeForRandom("From");
        int To = Instruments.getRangeForRandom("To");
        int i, j;

        for (i = 0; i < n; i++) {
            for (j = 0; j < m; j++) {
                SecondSequence[i][j] = Instruments.getRandomInt(From, To);
                if (i == 0) {
                    FirstSequence[j] = SecondSequence[i][j];
                }
            }
        }
        for (i = 0; i < n; i++) {
            System.out.print(Arrays.equals(SecondSequence[i], FirstSequence) + " ");
        }
    }

    //9 задание
    public static void findTheMostDistancePoint (int n) {
        double[][] ArrayOfGivenPoint = new double[n][2];
        double dis;
        double maxDis;

        int i, j, From = Instruments.getRangeForRandom("From"), To = Instruments.getRangeForRandom("To");

        System.out.println("Enter the coordinates of point");
        System.out.print("x = ");
        Scanner reader = new Scanner(System.in);
        double ThePointX = reader.nextDouble();
        System.out.print("y = ");
        double ThePointY = reader.nextDouble();
        System.out.println();
        for (i = 0; i < n; i++){
            System.out.print("Point " + (i + 1) + " : ");
            for (j = 0; j < 2; j++) {
                ArrayOfGivenPoint[i][j] = (From + (Math.random() * To));
                System.out.print(BigDecimal.valueOf(ArrayOfGivenPoint[i][j]).setScale(2, BigDecimal.ROUND_UP) + " ");
            }
            System.out.println();
        }
        maxDis = Math.sqrt(pow((ThePointX - ArrayOfGivenPoint[0][0]), 2) + pow((ThePointY - ArrayOfGivenPoint[0][1]), 2));
        for (i = 0; i < n; i++) {
            dis = Math.sqrt(pow((ThePointX - ArrayOfGivenPoint[i][0]), 2) + pow((ThePointY - ArrayOfGivenPoint[i][1]), 2));
            if (dis > maxDis) maxDis = dis;
        }
        System.out.println();
        System.out.print("The most distance is " + BigDecimal.valueOf(maxDis).setScale(2, BigDecimal.ROUND_UP));
    }

    //10 задание
    public static void calculateTheLengthOfTheBrokenLine(int n){
        int[][] Points = new int[n][2];
        System.out.println("Points are random or specific (R/S)?");
        Scanner reader = new Scanner(System.in);
        String check = reader.next();
        int i,j; double lengthLine = 0;

        if (check.contains("R")){
            Points = Instruments.generateArray(n,2);
            Instruments.writeArray(Points);
        }
        else {
            for (i = 0; i < n; i++){
                for (j = 0; j < 2; j++)
                    if (j == 0) {
                        System.out.println("Point " + (i + 1));
                        Points[i][j] = Instruments.readInteger("x = ");
                    }
                    else {
                        Points[i][j] = Instruments.readInteger("y = ");
                    }
            }
        }

        for (i = 0; i < (n - 1); i++){
                lengthLine = Math.sqrt(pow(Points[i][0] - Points[i+1][0],2) + pow(Points[i][1]-Points[i+1][1],2));
        }
        System.out.println("The lenght of line is " + BigDecimal.valueOf(lengthLine).setScale(2, BigDecimal.ROUND_UP));
    }
}

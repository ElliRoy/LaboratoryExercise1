package ru.isu.math;

import java.util.Scanner;
//В этом классе находятся методы, к которым часто приходилось обращаться
public class Instruments {
    public static int readInteger(String request) {
        System.out.print(request);
        Scanner reader = new Scanner(System.in);
        return reader.nextInt();
    }

    public static int getRandomInt(int a, int b) {
        return (a + (int) (Math.random() * b));
    }

    public static int getRangeForRandom(String Prmtr) {
        if (Prmtr.equals("From")) {
            System.out.println();
            return readInteger("Range of random. First of all enter number from: ");
        } else {
            return readInteger("Now enter number to: ");
        }
    }

    public static void writeArray(int[] a) {
        for (int anA : a) {
            System.out.print(anA + " | ");
        }
        System.out.println();
    }

    public static int[] generateArray1(int n) {
        int[] Ar = new int[n];
        int From = getRangeForRandom("From"), To = getRangeForRandom("To");
        for (int i = 0; i < n; i++) {
            Ar[i] = getRandomInt(From, To);
        }
        return Ar;
    }

    public static void writeArray(int[][] a){
        System.out.println();
        if (a.length == a[0].length) System.out.println("The square " + a.length + "th degree matrix:");
        else System.out.println("The " + a.length + " by " + a[0].length + " dimension matrix:");
        System.out.println();

        for (int i = 0; i < a.length; i++){
            for (int j = 0; j < a[0].length; j++){
                System.out.print(a[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static int[][] generateArray(int m, int n){
        int[][] Ar = new int[m][n];
        int From = getRangeForRandom("From"), To = getRangeForRandom("To");
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                Ar[i][j] = getRandomInt(From, To);
            }
        }
        return Ar;
    }
}
